import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, ResponseContentType } from '@angular/http';
import { PlatformLocation } from '@angular/common';
import { Subject } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import * as AWS from 'aws-sdk';
//const AWS = require("aws-sdk");

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {

    @ViewChild('pdfViewer') pdfViewer: ElementRef;

    public ip = 'https://pdf-viewer-service.herokuapp.com';
    public uploadUrl = this.ip + '/api/upload';
    public fileUrl = this.ip + '/pdf';
    public pdfSrc;
    public lastPage = 0;
    public wheelEvents;

    private _currentPage = 0;
    public set currentPage(page: number) {
        if (!page) {
            return;
        }
        const title = `Page | ${page}`;
        window.document.title = title;
        if (this._currentPage === page) {
            return;
        }
        this._currentPage = page;
        const url = `page/${page}`;
        window.history.pushState(null, '', url);
    }
    public get currentPage(): number {
        return this._currentPage;
    }

    x = 0;
    startX = 0;
    minX = -80;
    maxX = +80;

    constructor(
        public http: Http,
        public activeRoute: ActivatedRoute,
        public router: Router,
        public location: PlatformLocation
    ) {}

    ngOnInit() {
        this.getPdf();
        this.initCurrentPage();
        this.wheelEvents = new Subject();
        this.wheelEvents.pipe(
            throttleTime(100)
        ).subscribe((delta) => {
            this.currentPage += delta;
        });
    }

    onPanStart(event) {
      event.preventDefault();
      this.startX = this.x;
    }

    onPan(event) {
      event.preventDefault();
      if (Math.abs(this.x) < 40 && Math.abs(event.deltaX) < 40) {
          return;
      }
      this.x = this.startX + event.deltaX;
      if (this.x < this.minX) {
          this.x = this.minX;
      } else if (this.x > this.maxX) {
          this.x = this.maxX;
      }
    }

    onPanEnd(event) {
        if (this.x === this.maxX) {
            this.currentPage--;
        } else if (this.x === this.minX) {
            this.currentPage++;
        }
        this.x = 0;
    }

    getWheelDelta(event) {
        return event.ctrlKey ? +10 : +1;
    }

    mouseWheelUpFunc(event) {
        this.wheelEvents.next( - this.getWheelDelta(event));
    }

    mouseWheelDownFunc(event) {
        this.wheelEvents.next( + this.getWheelDelta(event));
    }

    getPdf() {
      AWS.config.credentials = new AWS.Credentials({
        accessKeyId: 'AKIAJZ36Z4M7OGXWCUAA', secretAccessKey: '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'
      });
      const params = {
        Bucket: 'JBeen',
        Key: 'Angular.pdf'
      };
      const s3 = new AWS.S3();
      s3.getObject(params, (err, data) => {
        if (err) {
          console.error(err); // an error occurred
        } else {
          const s = data.Body;
          this.pdfSrc = s;
          console.log(s);
        }
      });
    }

    initCurrentPage() {
        this.activeRoute.params.subscribe(params => {
            const id = params['id'] || 0;
            this.currentPage = +id;
        });
        this.location.onPopState(() => {
            const path = window.location.pathname;
            const id = path.split('/').pop();
            this.currentPage = +id;
        });
    }

    prevPage() {
        this.currentPage--;
    }

    nextPage() {
        this.currentPage++;
    }

    goLastPage() {
        this.currentPage = this.lastPage;
        this.lastPage = 0;
    }

    isFirstPage(): boolean {
        return this.currentPage <= 1;
    }

    onPageRendered(event) {
        const page = event.detail.pageNumber;
        if (page !== this.currentPage) {
            this.lastPage = this.currentPage;
            this.currentPage = page;
        }
    }

    lastButtonVisible(): boolean {
        return this.lastPage > 0;
    }

}
