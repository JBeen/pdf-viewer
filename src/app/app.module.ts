import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FileUploadModule } from 'ng2-file-upload';
import { ViewerComponent } from './viewer/viewer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { MouseWheelDirective } from './mousewheel.directive';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

export class MyHammerConfig extends HammerGestureConfig  {
    overrides = <any>{
        'swipe': {velocity: 0.4, threshold: 20} // override default settings
    };
}

const routes: Routes = [
    { path: 'page/:id', component: ViewerComponent },
    { path: '', redirectTo: '/page/1', pathMatch: 'full' },
];

@NgModule({
    declarations: [
        AppComponent,
        ViewerComponent,
        MouseWheelDirective,
    ],
    imports: [
        RouterModule.forRoot(routes),
        BrowserModule,
        PdfViewerModule,
        HttpModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatInputModule,
        FileUploadModule,
    ],
    bootstrap: [AppComponent],
    providers: [{
        provide: HAMMER_GESTURE_CONFIG,
        useClass: MyHammerConfig
    }]
})
export class AppModule { }
